import axios from 'axios'

export function request(config) {
    const instance = axios.create({
        baseURL: 'https://localhost:44304/api/blog',
        timeout: 5000
    })

    //请求拦截
    instance.interceptors.request.use(config => {
        config.headers.token = window.sessionStorage.getItem('token')
        return config
    }, err => {
        
    })

    //响应拦截
    instance.interceptors.response.use(res => {
        return res.data
    }, err => {

    })
    return instance(config)
}
