import { request } from '@/network/request'

export function addCate(cateInfo) {
    return request({
        url: '/addCate',
        method: 'POST',
        data: cateInfo
    })
}

export function getCates(uid) {
    return request({
        url: '/getCates',
        method: 'GET',
        params: {
            uid
        }
    })
}

export function getCateById(cId) {
    return request({
        url: '/getCate',
        method: 'GET',
        params: {
            cId
        }
    })
}

export function delCate(cId) {
    return request({
        url: '/deleteCate',
        params: {
            cId
        }
    })
}