import { request } from '@/network/request'

export function login(userInfo){
    return request({
        url:'/login',
        method:'POST',
        data:userInfo
    })
}

//获取用户身份认证信息
export function getUserIdentity(){
    return request({
        url:'/getUserInfo',
        method:'GET',
    })
}