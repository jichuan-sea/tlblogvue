var checkMobile = ((rule, phone, callBack) => {
    const phoneReg = /^[1][3,4,5,7,8][0-9]{9}$/
    if (phoneReg.test(phone)) return callBack()
    callBack(new Error("请输入合法的手机号"))
})

var loginRulesForm = {
    phone: [
        {
            required: true,
            message: '请输入手机号',
            trigger: 'blur'
        }, {
            validator: checkMobile
        }
    ],
    password: [
        {
            required: true,
            message: '请输入密码',
            trigger: 'blur'
        }
    ]
}

export { loginRulesForm }