var checkMobile = ((rule, phone, callBack) => {
    const phoneReg = /^[1][3,4,5,7,8][0-9]{9}$/
    if (phoneReg.test(phone)) return callBack()
    callBack(new Error("请输入合法的手机号"))
})

var checkPassword = ((rule, password, callBack) => {
    const passwordReg = new RegExp('(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{8,15}');
    if (passwordReg.test(password)) return callBack()
    callBack(new Error("您的密码复杂度太低（密码中必须包含字母、数字、特殊字符）"))
})

var registerRulesForm = {
    account: [
        {
            required: true,
            message: '请输入用户名',
            trigger: 'blur'
        },
        {
            min:3,
            max:12,
            message: '用户名字符在3-12个字符',
            trigger: 'blur'
        }
    ],
    phone: [
        {
            required: true,
            message: '请输入手机号',
            trigger: 'blur'
        }, {
            validator: checkMobile
        }
    ],
    validCode: [
        {
            required: true,
            message: '请输入验证码',
            trigger: 'blur'
        }
    ],
    password: [
        {
            required: true,
            message: '请输入密码',
            trigger: 'blur'
        },
        {
            min: 6,
            max: 12,
            message: '密码长度是8-15个字符',
            trigger: 'blur'
        }, {
            validator: checkPassword
        }
    ]
}

export { registerRulesForm }
