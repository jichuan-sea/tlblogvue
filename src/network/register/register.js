import { request } from '@/network/request'

export function registerApi(userInfo) {
    return request({
        url: '',
        methods: 'GET',
        data: userInfo
    })
}
