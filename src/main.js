import Vue from 'vue'
import App from './App.vue'
//导入
import router from './router/index'
import store from './store/index'

//全局引入element-ui.js
import ElementUI from '@/plugins/element-ui'

//全局样式
import '@/assets/css/element.css'
import '@/assets/css/global.css'
import '@/assets/css/normalize.css'

Vue.config.productionTip = false

//导入 vue-quill-edito
import VueQuillEditor from 'vue-quill-editor'

// 导入 vue-quill-edito / css
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

//注册全局  vue-quill-edito
Vue.use(VueQuillEditor)


new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
