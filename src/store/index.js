import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
    userInfo: {
        userId: 0 || localStorage.getItem('userId'),
        userName: '' || localStorage.getItem('userName')
    }
}

// const mutations ={
//     handleUserName(state, userInfo) {
//         // console.log(userInfo);
//         state.userInfo.userId = userInfo.userInfo.userid
//         state.userInfo.userName = userInfo.userInfo.username
//         localStorage.setItem('userId', state.userInfo.userId)
//         localStorage.setItem('userName', state.userInfo.userName)
//     }
// }

const store = new Vuex.Store({
    state,
    mutations: {
        handleUserName(state, userInfo) {
            // console.log(userInfo);

            state.userInfo.userId = userInfo.userInfo.userid
            state.userInfo.userName = userInfo.userInfo.username

            localStorage.setItem('userId', state.userInfo.userId)
            localStorage.setItem('userName', state.userInfo.userName)
        }
    },
    getters: {
        userId:(state)=> state.userInfo.userId,
        userName:(state)=> state.userInfo.userName,
    },
    actions: {},
    modules: {}
})

export default store
