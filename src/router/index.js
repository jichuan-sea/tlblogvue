import Vue from 'vue'
import VueRouter from 'vue-router'

// nprogess
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

const Login = () => import('@/pages/login/Login')
const Home = () => import('@/pages/home/Home')
const Register = () => import('@/pages/register/Register')
const Blog = () => import('@/pages/home/childrenCom/Blog')
const Publish = () => import('@/pages/home/childrenCom/Publish')

//安装
Vue.use(VueRouter)
//配置
const routes = [
    {
        path: '',
        redirect: '/login'
    },
    {
        path: '/login',
        component: Login
    },
    {
        path: '/register',
        component: Register
    },
    {
        path: '/home',
        component: Home,
        children: [
            {
                path: '',
                redirect: '/blog'
            },
            {
                path: '/blog',
                component: Blog
            },
            {
                path: '/publish',
                component: Publish
            }
        ]
    }
]

const router = new VueRouter({
    routes,
    mode: 'history'
})

router.beforeEach((to, from, next) => {
    if (to.path === '/login' || to.path === '/register') return next()
    const tokenStr = window.sessionStorage.getItem('token');
    if (!tokenStr) return next('/login')
    
    // console.log('start')
    NProgress.start()
    next()
})

router.afterEach(()=> {
    // console.log('done')
    NProgress.done()
})

//导出
export default router

